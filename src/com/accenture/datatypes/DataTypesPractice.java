package com.accenture.datatypes;

public class DataTypesPractice {
	public static void main(String args[]) {
	
	int myNum = 5;               // Integer (whole number)
	float myFloatNum = 5.99f;    // Floating point number
	double myDoubleNum = 19.99d;
	char myLetter = 'N';         // Character
	boolean myBool = true;       // Boolean
	String myText = "Nikhil";    
	
	System.out.println(myNum);
	System.out.println(myFloatNum);
	System.out.println(myDoubleNum);
	System.out.println(myLetter);
	System.out.println(myBool);
	System.out.println(myText);
}
}
